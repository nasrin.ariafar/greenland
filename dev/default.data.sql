
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_persian_ci DEFAULT NULL,
  `family` varchar(64) COLLATE utf8_persian_ci DEFAULT NULL,
  `username` varchar(128) COLLATE utf8_persian_ci DEFAULT NULL,
  `password` varchar(64) COLLATE utf8_persian_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;


DROP TABLE IF EXISTS `galleries`;
CREATE TABLE IF NOT EXISTS `galleries` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `title`             VARCHAR(500)     NULL,
    `description`       TEXT   NULL,
    `creationDate`      DATETIME            NULL,
    `updateDate`        DATETIME            NULL,
    `imageCount`        INT(4)   NOT NULL DEFAULT 0,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `title`             VARCHAR(500)     NULL,
    `shortDesc`         VARCHAR(100)    NULL,
    `longDesc`          TEXT   NULL,
    `creationDate`      DATETIME            NULL,
    `updateDate`        DATETIME            NULL,
    `notification`      BOOLEAN NOT NULL DEFAULT FALSE,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    `type`              TINYINT  NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `resources`;
CREATE TABLE IF NOT EXISTS `resources` (
    `id` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(64) NULL,
	`metaData` VARCHAR(64) NULL,
    `fileSize` VARCHAR(64) NULL,
    `fileType` VARCHAR(64) NULL,
	`parentId` INT(20) NULL,
	`title` VARCHAR(500) NULL,
	`location` VARCHAR(64) NULL,
	`description` TEXT NULL,
	`creationDate` DATETIME NULL,
    `updateDate` DATETIME NULL,
	`parentType` VARCHAR(64) NULL,
	`deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


 DROP TABLE IF EXISTS `educations`;
 CREATE TABLE IF NOT EXISTS `educations` (
     `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
     `title`             VARCHAR(500)     NULL,
     `description`       TEXT   NULL,
     `category`          TINYINT,
     `creationDate`      DATETIME            NULL,
     `updateDate`        DATETIME            NULL,
     `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
     PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


 DROP TABLE IF EXISTS `memories`;
 CREATE TABLE IF NOT EXISTS `memories` (
     `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
     `title`             VARCHAR(500)     NULL,
     `description`       TEXT   NULL,
     `creationDate`      DATETIME            NULL,
     `updateDate`        DATETIME            NULL,
     `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
     PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


 DROP TABLE IF EXISTS `stations`;
 CREATE TABLE IF NOT EXISTS `stations` (
     `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
     `type`             TINYINT NULL,
     `managerName`      VARCHAR(200)     NULL,
     `code`              VARCHAR(64)     NULL,
     `areaCode`          VARCHAR(64)     NULL,
     `regionCode`        VARCHAR(64)     NULL,
     `address`           VARCHAR(200)    NULL,
     `telNumberOne`      VARCHAR (15)    NULL,
     `telNumberTwo`      VARCHAR (15)    NULL,
     `fax`               VARCHAR (15)    NULL,
     `mobile`           VARCHAR (15)    NULL,
     `latitude`          VARCHAR (15)    NULL,
     `longitude`          VARCHAR (15)    NULL,
     `creationDate`      DATETIME            NULL,
     `updateDate`        DATETIME            NULL,
     `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
     PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


 DROP TABLE IF EXISTS `humans`;
 CREATE TABLE IF NOT EXISTS `humans` (
     `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
     `type`              TINYINT         NULL,
     `name`              VARCHAR(300)     NULL,
     `degree`            VARCHAR(64)     NULL,
     `experiences`       TEXT        NULL,
     `position`          VARCHAR(64)     NULL,
     `managerMessage`    TEXT   NULL,
     `emailAddress`         VARCHAR (128)    NULL,
     `telNumber`           TEXT NULL,
     `photoId`          INT(20) NULL,
     `startDate`     DATETIME        NULL,
     `endDate`     DATETIME        NULL,
     `creationDate`     DATETIME        NULL,
     `updateDate`        DATETIME        NULL,
     `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
     PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;



 DROP TABLE IF EXISTS `categories`;
 CREATE TABLE IF NOT EXISTS `categories` (
     `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
     `title`             VARCHAR(64)     NULL,
     `updateDate`        DATETIME            NULL,
     PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


 DROP TABLE IF EXISTS `evaluations`;
 CREATE TABLE IF NOT EXISTS `evaluations` (
     `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
     `title`             VARCHAR(64)     NULL,
     `status`            INT(1) NOT NULL DEFAULT 1,
     `winnerId`           VARCHAR(11),
     `winnerName`         VARCHAR(64)     NULL,
     `responseCount`     INT   NULL,
     `type`              VARCHAR(64)     NOT NULL,
     `creationDate`      DATETIME  NULL,
     `updateDate`        DATETIME            NULL,
     `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
     PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;



 DROP TABLE IF EXISTS `questions`;
 CREATE TABLE IF NOT EXISTS `questions` (
     `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
     `evaluationId`      INT(20),
     `question`          VARCHAR(64)     NULL,
     `type`              TINYINT,
     `answer`            TINYINT NULL,
     `responseCount`     TINYINT,
     `creationDate`      DATETIME            NULL,
     `updateDate`        DATETIME            NULL,
     `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
     PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1;


 DROP TABLE IF EXISTS `options`;
 CREATE TABLE IF NOT EXISTS `options` (
     `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
     `evaluationId`      INT(20),
     `questionId`        INT(20),
     `text`              VARCHAR(200) ,
     `value`             TINYINT,
     `responseCount`     INT(4),
     `statistics`        TINYINT,
     `ordering`        TINYINT NULL,
     `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
     PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


 DROP TABLE IF EXISTS `responses`;
 CREATE TABLE IF NOT EXISTS `responses` (
     `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
     `responderId`       VARCHAR(11),
     `responderName`     VARCHAR(200),
     `evaluationId`      INT(20),
     `questionId`        INT(20),
     `value`             TINYINT,
     `creationDate`        DATETIME,
     `updateDate`        DATETIME,
     `statistics`        DATETIME,
   PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


 DROP TABLE IF EXISTS `responders`;
 CREATE TABLE IF NOT EXISTS `responders` (
     `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
     `responderId`       VARCHAR(11),
     `responderName`     VARCHAR(200),
     `evaluationId`      INT(20),
     `trueAnswersCount`  INT(5),
   PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


 DROP TABLE IF EXISTS `devices`;
 CREATE TABLE IF NOT EXISTS `devices` (
     `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
     `type`              TINYINT,
     `deviceId`          VARCHAR(64)     NULL,
     `brand`             VARCHAR(64)     NULL,
     `model`             VARCHAR(64)     NULL,
     `manufacturer`      VARCHAR(64)     NULL,
     `version`           TINYINT,
     PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


 DROP TABLE IF EXISTS `reports`;
 CREATE TABLE IF NOT EXISTS `reports` (
     `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
     `description`       TEXT     NULL,
     `latitude`          FLOAT,
     `longitude`         FLOAT,
     `tell`              VARCHAR(20)  NULL,
     `creationDate`      DATETIME            NULL,
     `updateDate`        DATETIME            NULL,
     `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
     PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1000 ;


 DROP TABLE IF EXISTS `links`;
 CREATE TABLE IF NOT EXISTS `links` (
     `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
     `fileId`            INT(20) NULL,
     `title`             VARCHAR(200)  NULL,
     `description`       TEXT NULL,
     `path`              VARCHAR(500) NULL,
     `type`              TINYINT,
     `creationDate`      DATETIME            NULL,
     `updateDate`        DATETIME            NULL,
     `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
     PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


 DROP TABLE IF EXISTS `offices`;
 CREATE TABLE IF NOT EXISTS `offices` (
     `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
     `title`             VARCHAR(200)  NULL,
     `description`       TEXT NULL,
     `siteUrl`           VARCHAR(200)  NULL,
     `email`             VARCHAR(200)  NULL,
     `ceo`               VARCHAR(200)  NULL,
     `tell`              VARCHAR(200)  NULL,
     `fax`               VARCHAR(200)  NULL,
     `economicCode`      VARCHAR(200)  NULL,
     `creationDate`      DATETIME            NULL,
     `updateDate`        DATETIME            NULL,
     `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
     PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


 DROP TABLE IF EXISTS `questions_answers`;
 CREATE TABLE IF NOT EXISTS `questions_answers` (
     `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
     `type`              TINYINT,
     `askerName`         VARCHAR(200)  NULL,
     `askerFamily`       VARCHAR(200)  NULL,
     `askerEmail`        VARCHAR(200)  NULL,
     `title`             VARCHAR(200)  NULL,
     `question`          TEXT NULL,
     `answer`            TEXT NULL,
     `creationDate`      DATETIME NULL,
     `updateDate`        DATETIME NULL,
     `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
     PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;



 DROP TABLE IF EXISTS `markets`;
 CREATE TABLE IF NOT EXISTS `markets` (
     `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
     `parentId`          INT(20) NULL,
     `marketCode`        VARCHAR(200)  NULL,
     `name`              VARCHAR(200)  NULL,
     `establishDate`     DATETIME NULL,
     `activityType`      TINYINT,
     `boothCount`        TINYINT,
     `conexCount`        TINYINT,
     `kioskCount`        TINYINT,
     `placeCount`        TINYINT,
     `locationCount`     TINYINT,
     `frankishCount`     TINYINT,
     `customerCount`     TINYINT,
     `parking`           TINYINT,
     `tell`              VARCHAR(200)  NULL,
     `fax`               VARCHAR(200)  NULL,
     `isActive`          BOOLEAN NOT NULL DEFAULT TRUE,
     `address`           TEXT NULL,
     `latitude`          VARCHAR (15)    NULL,
     `longitude`         VARCHAR (15)    NULL,
     `creationDate`      DATETIME NULL,
     `updateDate`        DATETIME NULL,
     `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
     PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


 DROP TABLE IF EXISTS `categories`;
 CREATE TABLE IF NOT EXISTS `categories` (
     `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
     `title`             VARCHAR(500)     NULL,
     `creationDate`      DATETIME            NULL,
     `updateDate`        DATETIME            NULL,
     `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
     PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


 DROP TABLE IF EXISTS `tables`;
 CREATE TABLE IF NOT EXISTS `tables` (
     `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
     `categoryId`        INT(20) NULL,
     `description`       TEXT NULL,
     `creationDate`      DATETIME            NULL,
     `updateDate`        DATETIME            NULL,
     `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
     PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


 DROP TABLE IF EXISTS `fields`;
 CREATE TABLE IF NOT EXISTS `fields` (
     `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
     `tableId`           INT(20) NULL,
     `name`              VARCHAR(500)     NULL,
     `creationDate`      DATETIME            NULL,
     `updateDate`        DATETIME            NULL,
     `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
     PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


 DROP TABLE IF EXISTS `products`;
 CREATE TABLE IF NOT EXISTS `products` (
     `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
     `tableId`           INT(20) NULL,
     `categoryId`        INT(20) NULL,
     `values`            TEXT NOT NULL,
     `creationDate`      DATETIME            NULL,
     `updateDate`        DATETIME            NULL,
     `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
     PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


 DROP TABLE IF EXISTS `projects`;
 CREATE TABLE IF NOT EXISTS `projects` (
     `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
     `name`              TEXT  NULL,
     `aims`              TEXT  NULL,
     `actions`           TEXT  NULL,
     `progress`          TINYINT NULL,
     `location`          TINYINT NULL,
     `openingDate`       DATETIME            NULL,
     `creationDate`      DATETIME            NULL,
     `updateDate`        DATETIME            NULL,
     `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
     PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;
