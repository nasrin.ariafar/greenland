var $ = jQuery;
var SearchFlickr = {
    api_key	 : "e6eaf50b3abd2f025378bc3a655a320c",
    url	 : 'http://api.flickr.com/services/rest/?method=flickr.photos.getRecent',
    //				url	 : 'http://localhost/flickr/list.php',
    SelectPhotos : new Array()
}
SearchFlickr.Photo = function(){
    }
SearchFlickr.Photo.prototype = {
    id	: "",
    owner	: "",
    secret	: "",
    server	: "",
    farm	: null,
    title	: "",
    ispublic: null,
    isfriend: null,
    isfamily: null,
    selected: false,
    inserted: false,
    show	: function(el){
        
        src = "http://farm"+ this.farm +".static.flickr.com/"+ this.server +"/"+ this.id +"_"+ this.secret +"_" +this.size + ".jpg";
        $("<img/>").attr("src", src).appendTo(this);
        $(this).appendTo($(el));
        var bind = function(context, method) {
            return function() {
                return method.apply(context, arguments);
            };
        };
        
        this.bind('click', bind(this, this.select));
    },
    select : function(){
        if(this.selected){
            this.removeClass("selected");
            this.selected = false;
        }
        else{
            this.addClass("selected");
            this.selected = true;
        }
    }
				
}

SearchFlickr.Searcher = function(){
    photos  = new Array();
}
SearchFlickr.Searcher.prototype = {
    page	 : 1,
    pages	 : null,
    tag      : '',
    sort	 : '',
    per_page : 20,
    search	: function(config){
        
        this.config = config;
        this.tag = config.tag;
        //					this.sort = sort;
        //					this.per_page = per_page;
        var bind = function(context, method) {
            return function() {
                return method.apply(context, arguments);
            };
        };

        $.getJSON(
            SearchFlickr.url,
            {
                'api_key'	: SearchFlickr.api_key,
                'text'		: this.tag,
                //							'page'		: this.page,
                //							'per_page'	: this.per_page,
                'format'	: 'json',
                //							'sort'		: this.sort,
                'nojsoncallback': 1
            },
            bind(this, this.handleResponse)
            );
    },
    handleResponse : function(resp){
                    
        $("#images-box").html('');
        if(resp.stat == "ok"){
            this.pages = resp.photos.pages;
            this.page  = resp.photos.page;
            this.result = resp.photos.photo;
            
            //						this.updatePager();
            this.makeTable();
            this.updateModel();
        }
        
      
    },
    makeTable : function(obj){
        this.count = (obj && obj.count) ? obj.count : this.config.count;
        this.column = (obj && obj.column) ? obj.column : this.config.column;
        this.size = (obj && obj.size) ? obj.size : this.config.size;
        
        items = new Array();
        var container = $('#images-box');   
        container.empty();
        
        $("<table class='images-tbl'></table>").appendTo(container);
        var that = this;
        
        $.each(this.result, function (i, item){
            var index = i+ 1;
            if(index <= parseInt(that.count)){
               
                var tr;
                if((index % parseInt(that.column)) == 1){
                    tr = $("<tr></tr>");
                    $("table.images-tbl").append(tr);
                }
                photo = $.extend($('<td class="image"></td>'), new SearchFlickr.Photo());
                photo.id = item.id;
                photo.owner = item.owner;
                photo.secret = item.secret;
                photo.server = item.server;
                photo.farm = item.farm;
                photo.title = item.title;
                photo.ispublic = item.ispublic;
                photo.isfriend = item.isfriend;
                photo.isfamily = item.isfamily;
                photo.size = that.size;
                photo.show();
                $('.images-tbl tr:last').append(photo);
                items[i] = photo;
                
                if(that.config.frame == "on") {
                    photo.css('box-shadow','1px 2px 6px #888');
                } else{
                      photo.css('box-shadow','0 0 0');
                }
            }
        });
        
        setTimeout(function(){
             
            $('#loading').hide();
            $('#container').css('display' , 'block');
        },3000);
                   
        this.photos = items;
    },
    
    updateModel : function(){
        var model =  this.config.model;
        var count = this.config.count;
       
        model.set({
            photos : this.result.slice(0 ,count)//.slice(0, this.config.count)
        },{
            silent : true
        })
    },
    updatePager : function(){
        $("#current-page").html(this.page);
        $("#pages-count").html(this.pages);
        if(this.page == 1){
            $("#prev-button").attr('disabled', 'disabled');
        }
        else{
            $("#prev-button").attr('disabled', '');
        }
        if(this.page == this.pages){
            $("#next-button").attr('disabled', 'disabled');
        }
        else{
            $("#next-button").attr('disabled', '');
        }
    },
    nextPage: function(){
        var bind = function(context, method) {
            return function() {
                return method.apply(context, arguments);
            };
        };
        $.getJSON(
            SearchFlickr.url,
            {
                'api_key'	: SearchFlickr.api_key,
                'text'		: this.tag,
                'page'		: this.page + 1,
                'per_page'	: SearchFlickr.per_page,
                'format'	: 'json',
                'nojsoncallback': 1
            },
            bind(this, this.handleResponse)
            );
    },
    prevPage: function(){
        var bind = function(context, method) {
            return function() {
                return method.apply(context, arguments);
            };
        };
        $.getJSON(
            SearchFlickr.url,
            {
                'api_key'	: SearchFlickr.api_key,
                'text'		: this.tag,
                'page'		: this.page - 1,
                'per_page'	: SearchFlickr.per_page,
                'format'	: 'json',
                'nojsoncallback': 1
            },
            bind(this, this.handleResponse)
            );
    }
}


var flickrSrcher = new SearchFlickr.Searcher();

function showSelectPhotosCounter(){
    $("#show-selected-counter").html("<b>" + SearchFlickr.SelectPhotos.length + "</b>");
}