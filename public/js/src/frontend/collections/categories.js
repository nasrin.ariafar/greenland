define([
    'jquery',
    'underscore',
    'backbone',
    'base/collection/Collection',
    'frontend/models/category'
    ], function ($, _, Backbone, BaseCollections, ItemModel) {

        var CatgoriesCollection = BaseCollections.extend({

            model : ItemModel,

            initialize: function(models, opt){

                this.opt = opt;

                CatgoriesCollection.__super__.initialize.apply(this, [this.model, opt]);
            },
	
            url : function(){
                return "/api/categories" ;
            },

            parse: function( resp ){
                var target = this.target,
                list = resp.list;

                _.each(list, function(item){
                    item.target = target;
                    if(item.photos && item.photos.length > 0){
                        item.photos = $.grep(item.photos, function(item){
                            return (item.deleted != 1)
                        })
                    }
                });
                return list;
            }

        });

        return CatgoriesCollection;
    });
