define([
    'jquery',
    'underscore',
    'backbone',
    'base/collection/Collection',
    'frontend/models/table'
    ], function ($, _, Backbone, BaseCollections, ItemModel) {

        var TablesCollection = BaseCollections.extend({

            model : ItemModel,

            initialize: function(models, opt){

                this.opt = opt;

                TablesCollection.__super__.initialize.apply(this, [this.model, opt]);
            },
	
            url : function(){
                return "/api/categories/" + this.opt.categoryId + "/tables";
            },

            parse: function( resp ){
                var target = this.target,
                list = resp.list;

                _.each(list, function(item){
                    item.target = target;
                    if(item.photos && item.photos.length > 0){
                        item.photos = $.grep(item.photos, function(item){
                            return (item.deleted != 1)
                        })
                    }
                });
                return list;
            }

        });

        return TablesCollection;
    });
