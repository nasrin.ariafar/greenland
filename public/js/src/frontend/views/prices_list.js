define([
    'application',
    'jquery',
    'underscore',
    'backbone',
    'base/views/base_view',
    'text!frontend/templates/prices_list.html',
    'frontend/collections/categories',
    'frontend/collections/tables',
    'text!frontend/templates/category_item.html',
    'text!frontend/templates/table_item.html',
    'text!frontend/templates/product_item.html',
    'frontend/views/market_form',
    'frontend/views/item',
    'frontend/models/market',
    'frontend/models/product',
    'i18n!nls/fa-ir/labels',
    ], function (Application, $, _, Backbone, BaseView,  ListTpl, CategoriesCollection, TabelsCollection, CategoryTpl, TableTpl, ProductTpl, FormView, ItemView, ItemModel, ProductModel, Labels) {
       
        var NewsListView = BaseView.extend({

            className : "prices-list row",
            events : {
                'click .create-category'    : 'createCategory',
                'click .category-item'      : 'selectCategory',
                'keyup .new-field'          : 'addNewField',
                'click .create-table'       : 'createTable',
                'keyup td input'            : 'createUpdateProduct',
                'click .edit-product'       : 'switichToEditMode',
                'click .delete-product'     : 'deleteProduct'
            },

            initialize : function(){
                var $this = this;
                this.tableFields = [];
                this.categoriesCollection = new CategoriesCollection([]);
                this.categoriesCollection.fetch({
                    success : function(collection){
                        $this.selectedcategory = collection.at(0);
                        $this.getTabels(collection.at(0).get("id"));
                        collection.each(function(model, index){
                            $this.renderCategory(model, index);
                        });
                    }
                })
            },

            render : function(fields){
                var $this = this;

                this.template = _.template(ListTpl);
                this.$el.html(this.template( {
                    labels  : Labels
                }));

                return this;
            },

            registerEvents : function(){
                var $this = this;

                $('#new-category').on('hidden.bs.modal', function (e) {
                    $("#new-category input.new-category").val("");
                });

                $("#new-table").on('hidden.bs.modal', function (e) {
                    $("textarea.table-description", $this.$el).val("");
                    $(".fields-list", $this.$el).empty();
                    $(".new-field", $this.$el).val("");
                    $this.tableFields = [];
                });

            },

            renderCategory : function(category, index){
                var tpl = _.template(CategoryTpl);
                $('.categories-list', this.$el).append(tpl(_.extend(category.toJSON(),{
                    index : index
                })));
            },
            
            createCategory : function(){
                var title =   $("#new-category input.new-category").val(),
                $this = this;
                
                if (title != "")
                    this.categoriesCollection.create({
                        title : title
                    },{
                        async : false,
                        success : function(model){
                            $("#new-category", $this.$el).modal("hide");
                            $this.renderCategory(model);
                        }
                    })
            },

            selectCategory : function(e){
                var el = $(e.target).closest('.category-item'),
                categoryId = el.attr("id");

                if(!el.hasClass("selected")){
                    $(".categories-wrapper ul .selected").removeClass("selected");
                    el.addClass("selected");
                    $(".tables-list", this.$el).empty();
                    this.selectedcategory = this.categoriesCollection.get(categoryId);
                    this.getTabels(el.attr("id"));
                }

            },

            popNewItemCreation : function(model){
                var form_view = new FormView({
                    collection  : this.collection,
                    model       : (model && model instanceof Backbone.Model) ? model :  new ItemModel(),
                    className   : "modal-dialog"
                });

                $("#new-item-modal", this.$el).html(form_view.$el);
                $("#new-item-modal", this.$el).modal();
                form_view.afterRender();
            },

            popEditItem : function(e){
                var item_id = $(e.target).parents(".item-wrapper").attr("id"),
                model = this.collection.get(item_id);
                this.popNewItemCreation(model);
            },
            
            afterRenderItems : function(){
                var $this = this;

                if(this.collection.length == 0)
                    return;
            },

            getNewsByType : function(e){
                var el = $(e.target),
                type = el.data("type");

                if(!el.hasClass("selected")){
                    $(".second-navbar .selected", this.$el).removeClass("selected");
                    el.addClass("selected");
                    this.collection.filters.set("type", type);
                }
            },

            showFolderItems : function(e){
                var item_id = $(e.target).parents(".item-wrapper").attr("id"),
                parent = this.collection.get(item_id);
                
                $(".title", this.$el).html(Labels.square_bazaar_list + parent.get("name"))
                this.collection.filters.set("parentId", item_id);
                $(".back", this.$el).show();
            },

            back : function(e){
                this.collection.filters.set("parentId", 0);
                $(".back", this.$el).hide();
                $(".title", this.$el).html(Labels.squares_list);
            },

            addNewField : function(e){
                var field = $(".new-field", this.$el).val();
                if(field != "" && e.keyCode == 13){
                    this.tableFields.push(field);
                    $(".fields-list").append('<li>' + field + '</li>');
                    $(".new-field", this.$el).val("");
                }
            },

            getTabels : function(categoryId){
                var $this = this;
                
                this.tabelsCollection = new TabelsCollection([], {
                    categoryId : categoryId
                });
                this.tabelsCollection.fetch({
                    success : function(collection){
                        collection.each(function(model, index){
                            $this.renderTable(model, index);
                        });
                    }
                });
            },

            createTable : function(){
                var $this = this;
                if(!this.tableFields.length)
                    return;
                
                this.tabelsCollection.create({
                    categoryId : this.selectedcategory.get("id"),
                    description : $(".table-description").val(),
                    fields : this.tableFields
                },{
                    success : function(model){
                        $("#new-table").modal("hide");
                        $this.renderTable(model);
                    }
                });
            },

            renderTable : function(table){
                var tpl = _.template(TableTpl),
                $this = this;
                
                $('.tables-list', this.$el).append(tpl(_.extend(table.toJSON(),{
                    labels : Labels
                })));

                _.each(table.get("products"), function(product, index){
                    $this.renderRow(product, index)
                });
            },

            createUpdateProduct : function(e){
                var row = $(e.target).parents("tr"),
                tableId = row.data("table"),
                categoryId = row.data("category"),
                productId = row.attr("id"),
                values = [],
                product = new ProductModel(),
                $this = this,
                action = (productId) ? "edit" : 'create';
                
                if(e.keyCode != 13 || $("input[name=value]", row).val() == "" )
                    return;


                _.each($("form", row), function(form){
                    values.push($(form).serializeJSON());
                });

                product.save({
                    id      : productId,
                    tableId : tableId,
                    categoryId : categoryId,
                    values : JSON.stringify(values)
                },{
                    success : function(model){
                        if(action == "create"){
                            $("input:not(:hidden)", row).val("");
                            $this.renderRow(model.toJSON());
                        }else{
                            _.each($(".key-value", row), function(el){
                                var val =  $("input[name=value]", $(el)).val();
                                $("span.value", $(el)).show().html(val);
                                $("input[name=value]", $(el)).hide();
                            });
                        }
                    }
                })
                
            },

            renderRow : function(product, index){
                var tpl = _.template(ProductTpl);
                $('#' + product.tableId + ' tbody', this.$el).append(tpl(_.extend(product, {
                    index : (index) ? (index + 1) : $('#' + product.tableId + ' tbody tr', this.$el).length
                })));
            },

            switichToEditMode : function(e){
                var tr = $(e.target).parents("tr"),
                productId = tr.attr("id");
                _.each($(".key-value", tr), function(el){
                    $("span", $(el)).hide();
                    $("input[name=value]", $(el)).show();
                });
            },
            
            deleteProduct : function(e){
                var row = $(e.target).parents("tr"),
                product = new ProductModel({
                    id : row.attr("id"),
                    tableId :  row.data("table"),
                    categoryId : row.data("category")
                });

                product.destroy({
                    success : function(){
                        row.remove();
                    }
                })
            }

        });


        return NewsListView
    });
