define([
    'jquery',
    //    'libs/jquery-ui',
    'underscore',
    'backbone',
    'base/views/item_view',
    'text!modules/images/templates/image_item.html',
    'i18n!modules/images/nls/fa-ir/labels',
    ], function ($, _, Backbone, BaseItemView, ImageItemTpl, labels) {
        var resourcesAdminListItemView = BaseItemView.extend({
            tagName : 'li',
            className : 'photo-item',
            events    :
            {
                'click .icon-remove'                            : 'deleteImage',
                'click .caption'                                : 'openCaptionForm',
                'click  .galleryCaptionForm .save-caption'      : 'saveCaption',
                'click  .galleryCaptionForm  .cancel'           : 'closeCaptionForm'

            },

            initialize : function()
            {
                this.template = _.template(ImageItemTpl);
            },

            render    : function()
            {
                var obj = _.extend(this.model.toJSON(), {
                    labels : labels
                });
                this.$el.html(this.template(obj));

                return this;
            },

            deleteImage : function()
            {
                this.model.destroy();
                this.remove();
            },
            openCaptionForm : function(e)
            {
                $('.galleryCaptionForm').hide();
                
                var el = $('.galleryCaptionForm', this.$el)
                el.show();
                el.css({
                    top : e.clientY ,
                    left : e.clientX
                })

            },
            saveCaption : function()
            {
                var _this = this;
                
                this.model.set({
                    caption : $('.galleryCaptionForm', this.$el).serializeJSON()
                })
                this.closeCaptionForm();
            },
            closeCaptionForm : function()
            {
                $('.galleryCaptionForm', this.$el).hide();
                return false;

            }

        });

        return resourcesAdminListItemView;
    });
