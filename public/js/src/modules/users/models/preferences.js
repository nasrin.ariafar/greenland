define([
    'jquery',
    'backbone',
    'underscore'
], function ($, Backbone, _) {

    var prefModel = Backbone.Model.extend({
        defaults : {               
            id      : ''
        },

        url : function() {                 
            return '/api/users/' + this.get('id') + '/prefs';                
        }           
    });

    return prefModel;
});
