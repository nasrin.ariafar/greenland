define([
    'jquery',
    'backbone',
    'underscore',
    'base/views/item_view',
    'libs/jquery-helper',
    'modules/users/models/service',
    'modules/regions/collections/regions',
    'text!modules/users/templates/chargeService.html',
    'text!modules/regions/templates/region_item.html',
    'bootstrap_selectpicker',
    'i18n!modules/users/nls/edit-profile',
    'json!modules/users/config/price.json',
    'icheck'
    
    ], function ($, Backbone, _, ItemView, jqHelper, ServiceModel, RegionsCollection, tpl, RegionItemTpl, jQueryHelper, labels, PriceConstants) {

        var editProfile = ItemView.extend({
            
            className : 'charge-service-container',
                   
            events : {
                'click .save'               : 'save',
                //                'change input'              : 'getPrice',
                'change #acountDuration'    : 'getPrice',
                'change #serviceType'       : 'getPrice',
                'click .show-districts'     : 'showDistrictsListPopup'
            },
                    
            initialize : function(opt) {
                this.template = _.template(tpl);
                this.model = new ServiceModel();
                this.model.set('userId', window.currentUser.get('id'));
                this.model.fetch({
                    async : false
                })
                     
                this.render();
                this.c_model = this.model.clone();
            //                this.getDistrict();
            },
            
            render : function() {
                this.$el.html(this.template(_.extend(this.model.toJSON(), {
                    labels : labels
                })));
                return this;
            },

            afterRender : function(){
                this.getDistrict();
                var $this = this;
                
                $(".selectpicker").selectpicker({
                    width : "300x"
                });

                $('input', this.$el).iCheck({
                    checkboxClass: 'icheckbox_square',
                    radioClass: 'iradio_square',
                    increaseArea: '20%' // optional
                });

                $('input[type=checkbox]', this.$el).on('ifToggled', function(){
                    $this.getPrice($(this));
                });
                

                if(this.model.get('acountDuration'))
                    $('[name=acountDuration]').selectpicker('val', this.model.get('acountDuration') || 1 );

                if(this.model.get('serviceType'))
                    $('[name=serviceType]').selectpicker('val', this.model.get('serviceType') || 0 );
            },

            getDistrict : function(e){

                var regions,
                regionsCollection;

                regionsCollection = new RegionsCollection([],{
                    url : '/api/regions'
                });
                regionsCollection.fetch({
                    async: false
                });

                this.showRegions(regionsCollection);
            },
            
            showRegions : function(regionsCollection){
                var $this = this;
                
                regionsCollection.each(function(region){
                    var regionTpl = _.template(RegionItemTpl);
                    var obj = _.extend(region.toJSON(), {
                        labels : labels
                    })
                    
                    $('.regions-list' , $this.$el).append(regionTpl(obj));
                       
                });

                var regions = this.model.get('userRegions');
                if( regions ){
                    var userRegions = regions.split(',');
                    _.each(userRegions , function(region_id){
                        var el = $('input[data-id=' + region_id + ']');
                        if( $this.model.get('fuzzy_expire_date') > 0 ){
                            el.attr('checked', true);
                            el.attr('disabled', true);
                            $('label[for='+ region_id +']').addClass('disabled');
                        }
                    })
                }

            },

            save : function(){
                var $this = this;
                
                if( !this.bills || this.bills == 0 || $('input[type=checkbox]:checked:enabled', this.$el).length == 0 ){
                    if($('input:checked').length == 0)
                        $('.no-select-region').show();
                    return;
                }
                
                if( this.bills && this.bills > this.model.get('balanc')){
                    $('.lack_credit').show();
                    return;

                }
                    
                $('.lack_credit').hide();
                    
                var service = $('form.charg-service-form').serializeJSON();
                var regions = [];
                $('input[type=checkbox]:checked').each(function(index, el){
                    regions.push($(el).data('id'));
                });
                
                this.model.save(_.extend(service, {
                    userRegions : regions.join(),
                    userId      : window.currentUser.get('id'),
                    balanc      : this.model.get('balanc') - this.bills
                }), {
                    success : function(model){
                        if(model.get('serviceType') != 0){
                            window.location = "houses";
                        }else{
                            $this.render();
                            $this.afterRender();
                        }
                    }
                })
                   
            },

            calculate : function(regionCount, serviceType, acountDuration){
                var regions_first_class = [1,2,3,4,5,6,7,8,9,14,15];
                var sum = 0,
                firstClassPrice = 60,
                secondClassPrice = 40,
                f = 1,
                hasFirstClass = false;

                //define discount factor
                if(regionCount > 1 && regionCount < 10 ){
                    f = 0.4;
                }else if(regionCount >= 10 && regionCount < 20){
                    f = 0.3;
                }else if(regionCount >= 20 ){
                    f = 0.2;
                }

                //define base price based on device
                if(serviceType == 2){
                    firstClassPrice = 80;
                    secondClassPrice = 60;
                }

                _.each( $('input[type=checkbox]:checked:enabled', this.$el), function(el){
                    if(regions_first_class.indexOf(parseInt($(el).val())) != -1 ){
                        sum += firstClassPrice;
                        hasFirstClass = true;
                    }
                    else{
                        sum += secondClassPrice;
                    }
                });

                sum *= f;
                if(hasFirstClass){
                    sum += (1 - f) * firstClassPrice;
                }else{
                    sum += (1 - f) * secondClassPrice;
                }
                
                if(acountDuration < 3){
                //do nothing
                }else if(acountDuration < 6){
                    sum *= 0.9;
                }else if(acountDuration < 12){
                    sum *= 0.85;
                }else{
                    sum *= 0.8;
                }
                return sum * 1000;
                    
            },

            getPrice : function(e){
                var regions_count = $('input[type=checkbox]:checked:enabled', this.$el).length;
                
                if( regions_count > 0)
                    $('.no-select-region').hide()

                $('.price').hide();
              
                if( regions_count == 0 )
                    return;

                else{
                    var obj = $('.charg-service-form').serializeJSON();
                    var serviceType = obj.serviceType || this.model.get('serviceType');
                    var acountDuration =  obj.acountDuration || Math.round(this.model.get('fuzzy_expire_date') /30)
                    this.bills = this.calculate(regions_count, serviceType, acountDuration);
                    $('.price').show();
                    $('.total-price').html(_.translate(this.bills.toString(), true));
                }
            },
            showDistrictsListPopup  : function(e){
                $('.' + $(e.target).data('id')).modal();
            }
            
        });

        return editProfile;
    });
