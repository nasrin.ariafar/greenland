define([
    'jquery',
    'underscore',
    'backbone',
    'libs/jquery-ui',
    'modules/sites/models/site',
    'text!modules/users/templates/edit_website.html',
    'i18n!modules/sites/nls/labels'
    ], function ($, _, Backbone,j_ui ,siteModel, SiteTpl, labels) {
        
        require(['libs/jqueryPlugins/inputosaurus']);
        
        var EditSitesView = Backbone.View.extend({
            events : {
                'click .back-to-websites'   : 'backToWebsites',
                'click #edit-websites-save' : 'saveWebsite',
                'focusout   #site-domain'           : 'checkDomain',
                'focusout   #site-title'            : 'checkDomain',
                'keyup      #site-title'            : 'makeDomain',
                'keydown    #inputosaurus'          : 'addKeyword'
            },
            
            initialize : function(opt){
                var $this = this;
                this.model = new siteModel();
                this.model.set({
                    id : opt.site_id
                });
                this.model.fetch({
                    success: function(){
                        $this.render();
                    }
                });
            },
            
            render : function(){
                var template = _.template(SiteTpl);
                $('.websites-setting-detail-edit', this.$el).html(template({
                    labels : labels,
                    site: this.model.toJSON()
                }));
                $('.websites-setting-detail-edit', this.$el).show();
                $('.websites-setting-detail', this.$el).hide();
                $('.websites-setting-detail-privacy', this.$el).hide();
            },
            
            backToWebsites : function(){
                $('.websites-setting-detail', this.$el).show();
                $('.websites-setting-detail-edit', this.$el).hide();
                $('.websites-setting-detail-privacy', this.$el).hide();
            },
            
            saveWebsite : function(){
                var obj =  $('#edit-website').serializeJSON();
                var $this = this;
                this.model.save(obj, {
                    success : function(){
                        $('.success-setting', $this.$el).css('visibility', 'visible');
                        $('.error-setting', $this.$el).css('visibility', 'hidden');
                    },
                    error : function(){             
                        var error = $.parseJSON(arguments[1].responseText).fields;
                        $('.success-setting', $this.$el).css('visibility', 'hidden');
                        $('.error-setting', $this.$el).html('An error occurred, please try again.');
                        if(error.user && error.user.type == 'forbidden'){
                            $('.error-setting', $this.$el).html('You don\'t have access to edit this site');
                        }
                        $('.error-setting', $this.$el).css('visibility', 'visible');
                    }
                });
                
            },
            
            makeDomain : function(e)
            {
                var title = $(e.target).val();
                title = title.toLowerCase();
                $('#site-domain').val(title.replace(/[^a-z/^0-9]/g, ""));
            },
            
            checkDomain : function(e)
            {
                var $this = this,
                value = $(e.target).val();
                
                if($(e.target).val() != '' && this.model.get('domain') != value){
                    $.ajax({
                        url     : '/sites/sites/check-domain',                    
                        dataType : 'json',
                        data : {
                            domain : value
                        },
                        beforeSend : function(){
                            $('.check').addClass('hide');
                            $('.loading-image').removeClass('hide');
                            $('.loading-text').removeClass('hide');
                        },
                        success : function(resp){
                            $this.model.set('domain' , $('#site-domain').val());
                            $('.loading-image').addClass('hide');
                            $('.loading-text').addClass('hide');
                            if (resp) {
                                $('.new-site-domain').removeClass('error');
                                $('.check').removeClass('hide');

                            }else{
                                $('.new-site-domain').addClass('error');
                                $('.check').addClass('hide');
                            }
                        },
                        error : function(e, error){                        
                        }            
                    }, this);
                }
            },
            
            addKeyword : function(e){
                if ((e.keyCode == '13') || (e.keyCode == '32') || (e.keyCode == '188')){
                    $('#inputosaurus').inputosaurus();
                }
            }
        });

        return EditSitesView;
    });