<?php

class Tea_Controller_Plugin_Auth extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        return;
        $loginController  = 'auth';
        $loginAction      = 'login';
        $deniedController = 'auth';
        $deniedAction     = 'denied';

        $auth = Zend_Auth::getInstance();
        $acl  = Zend_Registry::getInstance()->get('acl');
        $role = $auth->hasIdentity() ? $auth->getIdentity()->getRole() : 'guest';
        $role = (empty($role) && $auth->hasIdentity()) ? 'user' : $role;

        $isAllowed = true;
        $resource  = $request->getModuleName()
            . ':' . $request->getControllerName()
            . ':' . $request->getActionName();

        $resource  = strtolower($resource);
        $isAllowed = $acl->isAccessible('system', $resource);

        if (!$isAllowed && !$auth->hasIdentity()) {

            if (   $request->getControllerName() != $loginController
                && $request->getActionName()     != $loginAction
            ) {
                $request->setModuleName('users');
                $request->setControllerName($loginController);
                $request->setActionName($loginAction);
            }

        } elseif (!$isAllowed && $auth->hasIdentity()) {

            if (   $request->getControllerName() != $deniedController
                && $request->getActionName()     != $deniedAction
            ) {
                $request->setModuleName('users');
                $request->setControllerName($deniedController);
                $request->setActionName($deniedAction);
            }
        }
    }
}
