<?php
//namespace Everyman\Neo4j;

/**
 * Represents an entity that is a collection of properties
 */
abstract class Tea_Neo4j_PropertyContainer
{
	protected $id = null;
	protected $client = null;
	protected $properties = null;

	protected $lazyLoad = true;

	/**
	 * Build the container and set its client
	 *
	 * @param Tea_Neo4j_Client $client
	 */
	public function __construct(Tea_Neo4j_Client $client)
	{
		$this->setClient($client);
	}

	/**
	 * Delete this entity
	 *
	 * @return Tea_Neo4j_PropertyContainer
	 * @throws Exception on failure
	 */
	abstract public function delete();

	/**
	 * Load this entity
	 *
	 * @return Tea_Neo4j_PropertyContainer
	 * @throws Exception on failure
	 */
	abstract public function load();

	/**
	 * Save this entity
	 *
	 * @return Tea_Neo4j_PropertyContainer
	 * @throws Exception on failure
	 */
	abstract public function save();

	/**
	 * Get the entity's client
	 *
	 * @return Tea_Neo4j_Client
	 */
	public function getClient()
	{
		return $this->client;
	}

	/**
	 * Get the entity's id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Return all properties
	 *
	 * @return array
	 */
	public function getProperties()
	{
		$this->loadProperties();
		return $this->properties;
	}

	/**
	 * Return the named property
	 *
	 * @param string $property
	 * @return mixed
	 */
	public function getProperty($property)
	{
		$this->loadProperties();
    	if ($property == 'id')
    		$property = '_id';
		
		return (isset($this->properties[$property])) ? $this->properties[$property] : null;
	}

	/**
	 * Is this entity identified?
	 *
	 * @return boolean
	 */
	public function hasId()
	{
		return $this->getId() !== null;
	}

	/**
	 * Remove a property set on the entity
	 *
	 * @param string $property
	 * @return PropertyContainer
	 */
	public function removeProperty($property)
	{
		$this->loadProperties();
		unset($this->properties[$property]);
		return $this;
	}

	/**
	 * Set the entity's client
	 *
	 * @param Tea_Neo4j_Client $client
	 * @return Tea_Neo4j_PropertyContainer
	 */
	public function setClient(Tea_Neo4j_Client $client)
	{
		$this->client = $client;
		return $this;
	}

	/**
	 * Set the entity's id
	 *
	 * @param integer $id
	 * @return PropertyContainer
	 */
	public function setId($id)
	{
		$this->id = $id === null ? null : (int)$id;
		return $this;
	}

	/**
	 * Set multiple properties on the entity
	 *
	 * @param array $properties
	 * @return Tea_Neo4j_PropertyContainer
	 */
	public function setProperties($properties)
	{
		$this->loadProperties();
		foreach ($properties as $property => $value) {
			$this->setProperty($property, $value);
		}
		return $this;
	}

	/**
	 * Set a property on the entity
	 *
	 * @param string $property
	 * @param mixed $value
	 * @return Tea_Neo4j_PropertyContainer
	 */
	public function setProperty($property, $value)
	{
		$this->loadProperties();
		if ($property == 'id')
			$property = '_id';
		if ($value === null) {
			$this->removeProperty($property);
		} else {
			$this->properties[$property] = $value;
		}
		return $this;
	}

	/**
	 * Should this entity be lazy-loaded if necessary?
	 *
	 * @param boolean $lazyLoad
	 * @return Tea_Neo4j_PropertyContainer
	 */
	public function useLazyLoad($lazyLoad)
	{
		$this->lazyLoad = (bool)$lazyLoad;
		return $this;
	}

	/**
	 * Set up the properties array the first time we need it
	 *
	 * This includes loading the properties from the server
	 * if we can get them.
	 */
	protected function loadProperties()
	{
		if ($this->properties === null) {
			$this->properties = array();
			if ($this->hasId() && $this->lazyLoad) {
				$this->load();
			}
		}
	}
	
	
    public function __get($property)
    {
    	if ($property == 'id')
    		$property = '_id';
    	return $this->getProperty($property);
    }


    public function __set($property, $value)
    {
    	if ($property == 'id')
    		$property = '_id';
    	$this->setProperty($property, $value);
    }

	
}
