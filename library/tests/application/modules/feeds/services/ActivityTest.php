<?php

class Feeds_Service_ActivityTest extends Tea_Test_ControllerTestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->cleanupTable('users');
        $this->cleanupTable('activities');
    }

    public function tearDown()
    {
    }

    public function testGetInstance()
    {
        $instance = Feeds_Service_Activity::getInstance();
        $this->assertInstanceOf('Feeds_Service_Activity', $instance);
    }

    public function testCreateActivity()
    {
        /*
         * activity is created
         * 
         * activity retrieves by id, 
         * and it's properties compare with original activity
         */
        
        $activityService = Feeds_Service_Activity::getInstance();
        
        $user       = $this->createTestUser();
        
        $activity = new Feeds_Model_Activity();
        $activity->setActorId($user->getId());
        $activity->setAction(0);
        $activity->setNew(true);
        $activity = $activityService->save($activity);
        
        $testActivity = $activityService->getByPK($activity->getId());
        $this->assertEquals($testActivity->getActorId(), $user->getId());
        $this->assertEquals($testActivity->getAction(), 0);
        $this->assertEquals($testActivity->getDeleted(), 0);
    }    

    public function testDeleteActivity()
    {
        /*
         * activity is created
         * activity is deleted
         * 
         * activity retrieves by id, 
         * and result of this search should be null
         */

        $activityService = Feeds_Service_Activity::getInstance();
        
        $user       = $this->createTestUser();
        $activity   = $this->createTestActivity($user);
        
        $activityService->remove($activity);
        $testActivity = $activityService->getByPK($activity->getId());
        $this->assertEquals($testActivity, null);
    }
}
