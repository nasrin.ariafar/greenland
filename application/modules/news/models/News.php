<?php

class News_Model_News extends Tea_Model_Entity
{

    const TYPE_OTHER_NEWS = 0;
    const TYPE_INTERNAL_NEWS = 1;
    const TYPE_ANNOUNCEMENTS = 2;
    const TYPE_PAPERCUTS = 3;
    const TYPE_FESTIVALS_NEWS = 4;
    const TYPE_PHOT0_COVERAGE = 5;
    const TYPE_STRATEGIES = 6;

    protected $_properties = array(
        'id' => null,
        'title' => null,
        'shortDesc' => null,
        'longDesc' => null,
        'creationDate' => null,
        'updateDate' => null,
        'deleted' => 0,
        'notification' => 0,
        'type' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'title' :
                case 'shortDesc':
                case 'longDesc':
                case 'type' :
                case 'creationDate' :
                case 'updateDate':
                case 'notification' :
                case 'deleted' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}

