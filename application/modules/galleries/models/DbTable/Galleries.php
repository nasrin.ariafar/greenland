<?php

class Galleries_Model_DbTable_Galleries extends Zend_Db_Table_Abstract
{
    protected $_name    = 'galleries';
    protected $_primary = 'id';
}
