<?php

class Memories_Model_DbTable_Memories extends Zend_Db_Table_Abstract
{
    protected $_name    = 'memories';
    protected $_primary = 'id';
}
