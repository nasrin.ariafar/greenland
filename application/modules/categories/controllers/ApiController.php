<?php

class Categories_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit = $this->_getParam('limit', 20);
        $start = $this->_getParam('start', 0);
        $deleted = $this->_getParam('deleted');

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);

        $categories_service = Categories_Service_Category::getInstance();

        $updateDate = $this->_getParam("updateDate");
        $filter = array();

        isset($deleted) && $filter["deleted"] = $deleted;
        isset($updateDate) && $filter["updateDate"] = $updateDate;

        $categories = $categories_service->getList($filter, $sort, $start, $count, $limit);
        $list = array();
        foreach ($categories as $item) {
            $item = $item->toArray();
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'list' => $list
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = Categories_Service_Category::getInstance();
        $gallery = $service->getByPK($id);

        if ($gallery)
            $result = $gallery->toArray();
        else
            $result = array();

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();
        $authService = Users_Service_Auth::getInstance();

        $user = $authService->getCurrentUser();
//        if (!isset($user)) {
//            $this->getResponse()
//                    ->setHttpResponseCode(404)
//                    ->appendBody('Unauthorized');
//            return;
//        }

        $gallery = new Categories_Model_Category();
        $gallery->setupdateDate('now');
        $gallery->fill($params);
        $result = Categories_Service_Category::getInstance()
                ->save($gallery);
        if ($result) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($result->toArray()));
            return;
        }
    }

    public function putAction()
    {
        $service = Categories_Service_Category::getInstance();
        $params = $this->getParams();

        $gallery = $service->getByPK($params['id']);
        //TO Do
        if (!$gallery instanceof Categories_Model_Category) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Category)');
            return;
        }

        $gallery->fill($params);
        $gallery->setUpdateDate('now');
        $gallery->setNew(false);
        $gallery = $service->save($gallery);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($gallery->toArray()));
    }

    public function deleteAction()
    {
        $service = Categories_Service_Category::getInstance();
        $galleryId = $this->_getParam('id');
        $gallery = $service->getByPK($galleryId);

        if (!$gallery instanceof Categories_Model_Category) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Category)");
            return;
        }

        $service->remove($gallery);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}

