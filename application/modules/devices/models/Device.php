<?php

class Devices_Model_Device extends Tea_Model_Entity
{

    protected $_properties = array(
        'id' => null,
        'deviceId' => null,
        'deviceType' => null,
        'brand' => null,
        'model' => null,
        'manufacturer' => null,
        'version' => null
    );

    public function __construct()
    {
        parent::__construct();
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'deviceId':
                case 'deviceType' :
                case 'brand' :
                case 'model' :
                case 'manufacturer' :
                case 'version' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

    public function toArray($forInsert = false)
    {
        $properties = array();

        foreach ($this->_properties as $key => $value) {
            $properties[$key] = $value;
        }

        return $properties;
    }

    public function getDeviceId()
    {
        return $this->_properties['deviceId'];
    }

}

