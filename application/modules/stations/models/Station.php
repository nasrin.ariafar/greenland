<?php

class Stations_Model_Station extends Tea_Model_Entity
{

    const TYPE_OFFICE = 1;
    const TYPE_BOOTH = 2;

    protected $_properties = array(
        'id' => NULL,
        'type' => NULL,
        'managerName' => NULL,
        'fax' => NULL,
        'mobile' => NULL,
        'code' => NULL,
        'areaCode' => NULL,
        'regionCode' => NULL,
        'address' => NULL,
        'telNumberOne' => NULL,
        'telNumberTwo' => NULL,
        'latitude' => NULL,
        'longitude' => NULL,
        'creationDate' => NULL,
        'updateDate' => NULL,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id':
                case 'type' :
                case 'managerName' :
                case 'fax' :
                case 'mobile' :
                case 'code':
                case 'areaCode' :
                case 'regionCode':
                case 'address' :
                case 'telNumberOne' :
                case 'telNumberTwo':
                case 'latitude':
                case 'longitude':
                case 'creationDate':
                case 'updateDate':
                case 'deleted':
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}