<?php

class Projects_Model_Project extends Tea_Model_Entity
{

    protected $_properties = array(
        'id' => NULL,
        'name' => NULL,
        'aims' => NULL,
        'actions' => NULL,
        'openingDate' => NULL,
        'progress' => NULL,
        'location' => NULL,
        'updateDate' => NULL,
        'creationDate' => NULL,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'name' :
                case 'aims' :
                case 'actions':
                case 'openingDate' :
                case 'progress' :
                case 'location':
                case 'updateDate';
                case 'creationDate';
                case 'deleted' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}

