<?php

class Default_CliController extends Zend_Controller_Action
{
    public function indexAction()
    {
        $script = null;
        $params = $this->_request->getParams();
        if ($params[0] == 'run-script') {
            $script = $params[1];
        }

        if ($script == null) {
            echo "use ./script/cli run-script [MODULE]:[SCRIPT]" . PHP_EOL;
            exit;
        }

        list($module, $script) = explode(":", $script);
        $script = str_replace('-', ' ', strtolower($script));
        $script = str_replace(' ', '', ucwords($script));
        $file   = APPLICATION_PATH . '/modules/' . $module . '/scripts/' . $script. '.php';
        $class  = ucfirst(strtolower($module)) . '_Script_' . $script;

        if (!file_exists($file)) {
            throw new Exception('The script file does not exist: ' . $script);
        }
        include_once $file;

        if (!class_exists($class)) {
            throw new Exception(
                'The script class: ' . $class.
                ' does not exist in file: ' . $file
            );
        }

        $inst = new $class($application->getBootstrap());
        $inst->execute();
        exit;
    }
}
