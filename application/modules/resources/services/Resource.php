<?php

class Resources_Service_Resource extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Resources_Model_DbTable_Resources();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, Resources_Model_Resource &$resource = null)
    {
        if ($ret = $this->staticGet($id)) {
            if ($resource instanceof Resources_Model_Resource) {
                $resource = $ret;
            }

            return $ret;
        }

        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$resource instanceof Resources_Model_Resource) {
            $resource = new Resources_Model_Resource();
        }
        $resource->fill($row);
        $resource->setNew(false);

        $this->staticSet($id, $resource);

        return $resource;
    }

    public function getImagesByParent($filter)
    {
        $select = $this->_table->select();

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                $select->where("{$key} = ?", $value);
            }
        }

        $cSelect = clone $select;
        $cSelect->from($this->_table, array('COUNT(*) AS count'));
        $rows = $this->_table->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $result = array();
        foreach ($this->_table->fetchAll($select) as $row) {
            $resource = new Resources_Model_Resource();
            $resource->fill($row);
            $resource->setNew(false);
            $result[] = $resource->toArray();
        }

        return $result;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->select();

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                if (is_array($value)) {
                    $select->where("{$key} IN (?)", $value);
                } else {
                    switch ($key) {
                        case 'name':
                        case 'file_type':
                            if ($value != '') {
                                $select->where("{$key} REGEXP ?", $value);
                            }
                            break;

                        default:
                            $select->where("{$key} = ?", $value);
                    }
                }
            }
        }

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $select->order($key . ' ' . strtoupper($ord));
            }
        }
        $cSelect = clone $select;
        $cSelect->from($this->_table, array('COUNT(*) AS count'));
        $rows = $this->_table->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $result = array();
        foreach ($this->_table->fetchAll($select) as $row) {
            $resource = new Resources_Model_Resource();
            $resource->fill($row);
            $resource->setNew(false);
            $result[] = $resource;
        }

        return $result;
    }

    public function save(Resources_Model_Resource $resource)
    {
        if ($resource->isNew()) {
            $data = $resource->toArray(true);
            $data['metaData'] = serialize($data['metaData']);
            $pk = $this->_table->insert($data);

            if ($pk) {
                $resource = $this->getByPK($pk, $resource);
                $this->staticSet($resource->getId(), $resource);

                return $resource;
            }
        } else {
            $id = $resource->getId();
            $data = $resource->toArray(true);
            $data['metaData'] = serialize($data['metaData']);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            $resource = $this->getByPK($id, $resource);
            return $resource;
        }

        return false;
    }

    public function resizeImage($resource, $width, $height, $background = false)
    {
        if (is_string($resource)) {
            $resource = $this->getByPK($resource);
        }

        if (!$resource instanceof Resources_Model_Resource) {
            return false;
        }

        if ($resource->getReferenceType() == Resources_Model_Resource::REFERENCE_TYPE_CONTAINER) {
            $resIds = unserialize($resource->getReference());
            $size = $width . 'x' . $height;
            if (isset($resIds[$size])) {
                return $resIds[$size];
            }
        }

        if ($background) {
            Tea_Service_Gearman::getInstance()->doBackground(
                    'resources', 'create_thumbnails', array(
                'resource' => $resource,
                'size' => $width . 'x' . $height
                    )
            );
            return true;
        } else {
            return Tea_Service_Gearman::getInstance()->do(
                            'resources', 'create_thumbnails', array(
                        'resource' => $resource,
                        'size' => $width . 'x' . $height
                            )
            );
        }

        return false;
    }

    public function remove(Resources_Model_Resource $resource)
    {
        $this->staticDel($resource->getId());
        $file = $resource->getReference();
        $file = realpath(APPLICATION_PATH . '/../public/' . $file);
        if (is_file($file)) {
            unlink($file);
        }

//        if ($resource->getReferenceType() == Resources_Model_Resource::REFERENCE_TYPE_FILE) {
//            $file = $resource->getReference();
//            $file = realpath(APPLICATION_PATH . '/../public/' . $file);
//            if (is_file($file)) {
//                unlink($file);
//            }
//        } elseif ($resource->getReferenceType() == Resources_Model_Resource::REFERENCE_TYPE_CONTAINER) {
//            $resIds = unserialize($resource->getReference());
//            foreach ($resIds as $resId) {
//                $res = $this->getByPK($resId);
//                if ($res instanceof Resources_Model_Resource) {
//                    $this->remove($res);
//                }
//            }
//        }

        $where = $this->_table->getAdapter()
                ->quoteInto('id = ?', $resource->getId());
        $this->_table->delete($where);
    }

    public function removeResourcesId($resourceIds)
    {
        foreach ($resourceIds as $resourceId) {
            $resource = $this->getByPK($resourceId);
            $this->remove($resource);
        }
    }

}
