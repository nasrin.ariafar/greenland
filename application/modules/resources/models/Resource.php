<?php

class Resources_Model_Resource extends Tea_Model_Entity
{

    const PARENT_TYPE_NEWS = 'news';
    const PARENT_TYPE_EDUCATION = 'education';
    const PARENT_TYPE_HUMAN = 'human';
    const PARENT_TYPE_GALLERY = 'gallery';
    const PARENT_TYPE_REPORT = 'report';

    protected $_properties = array(
        'id' => null,
        'name' => null,
        'fileSize' => null,
        'fileType' => null,
        'metaData' => null,
        'parentId' => NULL,
        'title' => NULL,
        'location' => NULL,
        'description' => NULL,
        'updateDate' => NULL,
        'creationDate' => NULL,
        'parentType' => NULL,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'name' :
                case 'fileSize' :
                case 'fileType':
                case 'metaData':
                case 'parentId':
                case 'title' :
                case 'location' :
                case 'description' :
                case 'creationDate' :
                case 'updateDate' :
                case 'parentType' :
                case 'deleted' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

    public function setMetaData($meta)
    {
        $this->_properties['metaData'] = serialize($meta);
    }

    public function getMetaData()
    {
        return @unserialize($this->_properties['metaData']);
    }

//    public function getUrl()
//    {
//        $url = '/r/' . $this->_properties['id'];
//
//        if (($this->_properties['reference_type'] == Resources_Model_Resource::REFERENCE_TYPE_FILE)
//                || ($this->_properties['reference_type'] == Resources_Model_Resource::REFERENCE_TYPE_URL)
//                || ($this->_properties['reference_type'] == Resources_Model_Resource::REFERENCE_TYPE_YOUTUBE)
//        ) {
//            $url = $this->_properties['reference'];
//        }
//
//        return $url;
//    }

    public function setParentId($parentId)
    {
        $this->_parentId = $parentId;
        $this->_properties['parentId'] = $parentId;
    }

    public function setParentType($parentType)
    {
        $this->_parentType = $parentType;
        $this->_properties['parentType'] = $parentType;
    }

//    public function getCreationDate()
//    {
//        return $this->_properties['creationDate'];
//    }
}
