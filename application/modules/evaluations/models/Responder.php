<?php

class Evaluations_Model_Responder extends Tea_Model_Entity
{

    protected $_properties = array(
        'id' => null,
        'responderId' => null,
        'responderName' => null,
        'evaluationId' => null,
        'trueAnswersCount' => null
    );

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'responderId' :
                case 'responderName' :
                case 'evaluationId' :
                case 'trueAnswersCount':
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}
