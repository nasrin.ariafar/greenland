<?php

class Evaluations_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $evaluationService = Evaluations_Service_Evaluation::getInstance();

        $limit = $this->_getParam('limit', 10);
        $start = $this->_getParam('start', 0);
        $updateDate = $this->_getParam("updateDate");
        $deleted = $this->_getParam('deleted');
        $type = $this->_getParam('type');

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);

        $filter = array();

        isset($deleted) && $filter['deleted'] = $deleted;
        isset($type) && $filter['type'] = $type;
        isset($updateDate) && $filter['updateDate'] = $updateDate;

        $dt = new DateTime($str);
        $dt->setTimeZone(new DateTimeZone('Asia/Tehran'));

        $evaluations = $evaluationService->getList($filter, $sort, $start, $count, $limit);

        $list = array();
        foreach ($evaluations as $evaluation) {
            $_avaluation = $evaluation->toArray();
            $_avaluation['questions'] = array();
            foreach ($evaluation->getQuestions() as $question) {
                $_question = $question->toArray();
                $_question['items'] = array();
                foreach ($question->getItems() as $item) {
                    $_item = $item->toArray();
                    $_question['items'][] = $_item;
                }
                $_avaluation['questions'][] = $_question;
            }
            $list[] = $_avaluation;
        }

        $result = array(
            'count' => $count,
            'list' => $list,
            'updateDate' => $dt->format('Y-m-d H:i:s')
        );

        $response = json_encode($result);
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody($response);
    }

    public function getAction()
    {
        $evaluationService = Evaluations_Service_Evaluation::getInstance();

        $deleted = $this->getParam('deleted');
        $filter = array();
        isset($deleted) && $filter['deleted'] = $deleted;

        $evaluationId = $this->_getParam('id');
        $evaluation = $evaluationService->getByPK($evaluationId, null, $filter);
        if (!$evaluation instanceof Evaluations_Model_Evaluation) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Evaluation)");
            return;
        }

        $_avaluation = $evaluation->toArray();
        $_avaluation['questions'] = array();
        foreach ($evaluation->getQuestions() as $question) {
            $_question = $question->toArray();
            $_question['items'] = array();
            foreach ($question->getItems() as $item) {
                $_item = $item->toArray();
                $_question['items'][] = $_item;
            }
            $_avaluation['questions'][] = $_question;
        }

//        if ($evaluation->getStatus() == 1) {
            $results = $evaluationService->calculateStatistics($evaluation);
            $_avaluation["response"] = $results;
//        }


        $response = json_encode($_avaluation);
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody($response);
    }

    public function postAction()
    {
        $evaluationService = Evaluations_Service_Evaluation::getInstance();

        if ($this->_currentUser == null) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (User)");
            return;
        }

        $valid = $this->checkParams(array(
            'title'
                ));
        if (!$valid) {
            return;
        }

        $params = $this->getParams();

        $evaluation = new Evaluations_Model_Evaluation();
        $evaluation->fill($params);
//        $evaluation->setCreatedById($this->_currentUser->getId());
//        $evaluation->setUpdatedById($this->_currentUser->getId());
        $evaluation->setNew(true);

        $questions = array();
        foreach ($params['questions'] as $questionParams) {
            $questionRequiredParams = array(
                'question'
            );

            foreach ($questionRequiredParams as $reqParam) {
                if (!isset($questionParams[$reqParam])) {
                    $this->getResponse()
                            ->setHttpResponseCode(404)
                            ->appendBody("Not Found " . $reqParam);
                    return;
                }

                if (is_string($questionParams[$reqParam])) {
                    if (strlen(trim($questionParams[$reqParam])) == 0) {
                        $this->getResponse()
                                ->setHttpResponseCode(404)
                                ->appendBody("Not Found " . $reqParam);
                        return;
                    }
                }
            }

            $question = new Evaluations_Model_Question();
            $question->fill($questionParams);
//            $question->setCreatedById($this->_currentUser->getId());
//            $question->setUpdatedById($this->_currentUser->getId());
            $question->setNew(true);

            $items = array();
            foreach ($questionParams['items'] as $itemParams) {

                $itemRequiredParams = array(
                    'text',
                    'value'
                );

                foreach ($itemRequiredParams as $reqParam) {
                    if (!isset($itemParams[$reqParam])) {
                        $this->getResponse()
                                ->setHttpResponseCode(404)
                                ->appendBody("Not Found " . $reqParam);
                        return;
                    }

                    if (is_string($itemParams[$reqParam])) {
                        if (strlen(trim($itemParams[$reqParam])) == 0) {
                            $this->getResponse()
                                    ->setHttpResponseCode(404)
                                    ->appendBody("Not Found " . $reqParam);
                            return;
                        }
                    }
                }

                $item = new Evaluations_Model_QuestionItem();
                $item->fill($itemParams);
//                $item->setCreatedById($this->_currentUser->getId());
//                $item->setUpdatedById($this->_currentUser->getId());
                $item->setNew(true);

                $items[] = $item;
            }

            $question->setItems($items);

            $questions[] = $question;
        }

        $evaluation->setQuestions($questions);

        $evaluation = $evaluationService->saveCompleteEvaluation($evaluation);

        $_avaluation = $evaluation->toArray();
        $_avaluation['questions'] = array();
        foreach ($evaluation->getQuestions() as $question) {
            $_question = $question->toArray();
            $_question['items'] = array();
            foreach ($question->getItems() as $item) {
                $_item = $item->toArray();
                $_question['items'][] = $_item;
            }
            $_avaluation['questions'][] = $_question;
        }

        $response = json_encode($_avaluation);
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody($response);
    }

    public function putAction()
    {
        $evaluationService = Evaluations_Service_Evaluation::getInstance();

        $user = $this->_currentUser;
        if ($user == null) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (User)");
            return;
        }
        $evaluationId = $this->_getParam('id');
        $evaluation = $evaluationService->getByPK($evaluationId);
        if (!$evaluation instanceof Evaluations_Model_Evaluation) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Evaluation)");
            return;
        }

        $invariableParams = array(
            'ref_type',
            'ref_id',
            'assignee_count',
            'response_count',
            'creationDate'
        );

        $params = $this->getParams();

        foreach ($invariableParams as $invariableParam) {
            if (isset($params[$invariableParam])) {
                unset($params[$invariableParam]);
            }
        }


        if (isset($params['status']) && $params['status'] == 1) {
            $winner = $evaluationService->getWinner($evaluation);
            $e_winner = $winner[0]->toArray();
            $params['winnerId'] = $e_winner['responderId'];
            $params['winnerName'] = $e_winner['responderName'];
        }

        $evaluation->fill($params);
        $evaluation->setNew(false);
        $evaluation->setUpdateDate('now');
        $evaluation = $evaluationService->save($evaluation);

        if (!$evaluation) {
            $this->getResponse()
                    ->setHttpResponseCode(400)
                    ->appendBody("Evaluation Not modified");
            return;
        } else {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($evaluation));
        }
    }

    public function deleteAction()
    {
        $evaluationService = Evaluations_Service_Evaluation::getInstance();

        $evaluationId = $this->_getParam('id');
        $evaluation = $evaluationService->getByPK($evaluationId);
        if (!$evaluation instanceof Evaluations_Model_Evaluation) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Evaluation)");
            return;
        }

        $evaluationService->remove($evaluation);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}