SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `polls`;
--
-- Table structure for table 'polls'
--

CREATE TABLE IF NOT EXISTS polls  (

    id              INT(11)         NOT NULL AUTO_INCREMENT,
    title           VARCHAR(1024)   NOT NULL,
    description     Text	,
    ref_type        INT(4)	,
    ref_id          INT         ,
    status          INT(1)          NOT NULL,
    privacy         INT(1)          NOT NULL,
    start_date      DATETIME	,
    end_date        DATETIME	,
    assignee_count  INT(4)	,
    response_count  INT(4)          NOT NULL,
    created_by_id   INT             NOT NULL,
    creation_date   DATETIME        NOT NULL,
    updated_by_id   INT             NOT NULL,
    update_date     DATETIME        NOT NULL,

    PRIMARY KEY(id)
)   ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `poll_votings`;
--
-- Table structure for table 'poll_votings'
--

CREATE TABLE IF NOT EXISTS poll_votings (
    id                  INT             NOT NULL    AUTO_INCREMENT,
    poll_id             INT             NOT NULL,
    question            VARCHAR(1024)   NOT NULL,
    response_count      INT(4)          NOT NULL,
    weight              INT(2)          NOT NULL,
    ordering            INT(2)          NOT NULL,
    created_by_id       INT             NOT NULL,
    creation_date       DATETIME        NOT NULL,
    updated_by_id       INT             NOT NULL,
    update_date         DATETIME        NOT NULL,

    PRIMARY KEY (id)
)   ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `polls_voting_items`;
--
-- Table structure for table 'polls_voting_items'
--

CREATE TABLE IF NOT EXISTS poll_voting_items (
    id                  INT             NOT NULL    AUTO_INCREMENT,
    voting_id           INT             NOT NULL,
    poll_id             INT             NOT NULL,
    text                VARCHAR(1024)	NOT NULL,
    value               INT(2),
    ordering            INT(2)          NOT NULL,
    response_count	INT(4)          NOT NULL,
    created_by_id	INT             NOT NULL,
    creation_date	DATETIME	NOT NULL,

    PRIMARY KEY (id)
)   ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `poll_assignees`;
--
-- Table structure for table 'poll_assignees'
--

CREATE TABLE IF NOT EXISTS poll_assignees (
    assignee_id 	INT         NOT NULL                ,
    poll_id	INT         NOT NULL,
    status              INT(1)      NOT NULL,
    created_by_id	INT         NOT NULL,
    creation_date	DATETIME    NOT NULL,
    updated_by_id	INT         NOT NULL,
    update_date         DATETIME    NOT NULL,

    PRIMARY KEY (assignee_id,poll_id)
)   ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `poll_voting_responses`;
--
-- Table structure for table 'poll_voting_responses'
--

CREATE TABLE IF NOT EXISTS poll_voting_responses (
    responder_id	INT     	NOT NULL,
    voting_id           INT     	NOT NULL,
    poll_id             INT     	NOT NULL,
    value               VARCHAR(50)	NOT NULL,
    created_by_id	INT         NOT NULL,
    creation_date	DATETIME    NOT NULL,
    updated_by_id	INT         NOT NULL,
    update_date         DATETIME    NOT NULL,

    PRIMARY KEY (responder_id, voting_id, value)
)   ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


