<?php

class Polls_Hook_UpdateStats
{

    public function execute($event, $data)
    {
        switch ($event) {
            case 'response_poll':
            case 'delete_response':
                if (!is_array($data) || 
                    !isset($data['poll']) ||
                    !$data['poll'] instanceof Polls_Model_Poll) {
                    
                    return;
                }

                $this->_updateResponseCount($data['poll']);

                break;
        }
    }
    
    private function _updateResponseCount(Polls_Model_Poll $poll)
    {
        $pollService            = Polls_Service_Poll::getInstance();
        $votingService          = Polls_Service_Voting::getInstance();
        $votingItemService      = Polls_Service_VotingItem::getInstance();
        $votingResponseService  = Polls_Service_VotingResponse::getInstance();
        
        $poll = $pollService->getByPK($poll->getId());
        
        $respCount = $votingResponseService->countPollResponses($poll);
        $poll->setResponseCount($respCount);
        $poll->setNew(false);
        $pollService->save($poll);
        
        $votings = $poll->getVotings();
        foreach ($votings as $voting) {
            $respCount = $votingResponseService->countVotingResponses($voting);
            $voting->setResponseCount($respCount);
            $voting->setNew(false);
            $votingService->save($voting);
            
            $items = $voting->getItems();
            foreach ($items as $item) {
                $respCount = $votingResponseService->countItemResponses($item);
                $item->setResponseCount($respCount);
                $item->setNew(false);
                $votingItemService->save($item);
            }
        }
    }
    
}
