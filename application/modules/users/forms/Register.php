<?php

class Users_Form_Register extends Zend_Form
{
    public function init()
    {
        $firstName = new Zend_Form_Element('first_name');
        $firstName->setRequired(true);        
        $this->addElement($firstName);
        
        $lastName = new Zend_Form_Element('last_name');
        $lastName->setRequired(true);
        $this->addElement($lastName);
        
        $email = new Zend_Form_Element('email');
        $email->setRequired(true)
                ->addValidator(new Zend_Validate_EmailAddress())
                ->addValidator(new Users_Validate_NotDuplicate());
        $this->addElement($email);
        
        $password = new Zend_Form_Element('password');
        $password->setRequired(true)
                ->addFilter(new Zend_Filter_StringTrim())
                ->addValidator(new Zend_Validate_StringLength(6, 20));
        $this->addElement($password);
        
        $confirmPass = new Zend_Form_Element_Password('confirm_password');
        $confirmPass->setRequired(true)
                ->addFilter(new Zend_Filter_StringTrim())
                ->addValidator(new Zend_Validate_Identical('password'));
        $this->addElement($confirmPass);
        
        $userName = new Zend_Form_Element('username');
        $userName->setRequired(false);
        $this->addElement($userName);
        
        $gender = new Zend_Form_Element('gender');
        $gender->setRequired(false);
        $this->addElement($gender);
        
        $photoId = new Zend_Form_Element('photo_id');
        $photoId->setRequired(false);
        $this->addElement($photoId);
        
        $nationalCode = new Zend_Form_Element('national_code');
        $nationalCode->setRequired(false);
        $this->addElement($nationalCode);
        
        $birthDate = new Zend_Form_Element('birth_date');
        $birthDate->setRequired(false);
        $this->addElement($birthDate);
        
        $timezone = new Zend_Form_Element('timezone');
        $timezone->setRequired(false);
        $this->addElement($timezone);
        
        $locale = new Zend_Form_Element('locale');
        $locale->setRequired(false);
        $this->addElement($locale);
        
        $profileId = new Zend_Form_Element('profile_id');
        $profileId->setRequired(false);
        $this->addElement($profileId);
        
        $status = new Zend_Form_Element('status');
        $status->setRequired(false);
        $this->addElement($status);
        
        $createdById = new Zend_Form_Element('created_by_id');
        $createdById->setRequired(false);
        $this->addElement($createdById);
        
        $creationDate = new Zend_Form_Element('creation_date');
        $creationDate->setRequired(false);
        $this->addElement($creationDate);
        
        $updatedById = new Zend_Form_Element('updated_by_id');
        $updatedById->setRequired(false);
        $this->addElement($updatedById);
        
        $updateDate = new Zend_Form_Element('update_date');
        $updateDate->setRequired(false);
        $this->addElement($updateDate);
        
        //profile properties
        $firstNameE = new Zend_Form_Element('first_name_e');
        $firstNameE->setRequired(false);
        $this->addElement($firstNameE);
        
        $middleNameE = new Zend_Form_Element('middle_name_e');
        $middleNameE->setRequired(false);
        $this->addElement($middleNameE);
        
        $lastNameE = new Zend_Form_Element('last_name_e');
        $lastNameE->setRequired(false);
        $this->addElement($lastNameE);
        
        $fatherName = new Zend_Form_Element('father_name');
        $fatherName->setRequired(false);
        $this->addElement($fatherName);
        
        $birthLoc = new Zend_Form_Element('birth_loc');
        $birthLoc->setRequired(false);
        $this->addElement($birthLoc);
        
        $identityNo = new Zend_Form_Element('identity_no');
        $identityNo->setRequired(false);
        $this->addElement($identityNo);
        
        $identityDate = new Zend_Form_Element('identity_date');
        $identityDate->setRequired(false);
        $this->addElement($identityDate);
        
        $identityLoc = new Zend_Form_Element('identity_loc');
        $identityLoc->setRequired(false);
        $this->addElement($identityLoc);
        
        $cell = new Zend_Form_Element('cell');
        $cell->setRequired(false);
        $this->addElement($cell);
        
        $cell2 = new Zend_Form_Element('cell2');
        $cell2->setRequired(false);
        $this->addElement($cell2);
        
        $phone = new Zend_Form_Element('phone');
        $phone->setRequired(false);
        $this->addElement($phone);
        
        $country = new Zend_Form_Element('country');
        $country->setRequired(false);
        $this->addElement($country);
        
        $state = new Zend_Form_Element('state');
        $state->setRequired(false);
        $this->addElement($state);
        
        $city = new Zend_Form_Element('city');
        $city->setRequired(false);
        $this->addElement($city);
        
        $street = new Zend_Form_Element('street');
        $street->setRequired(false);
        $this->addElement($street);
        
        $zipCode = new Zend_Form_Element('zip_code');
        $zipCode->setRequired(false);
        $this->addElement($zipCode);
        
        $homePhone = new Zend_Form_Element('home_phone');
        $homePhone->setRequired(false);
        $this->addElement($homePhone);
        
        $homeCountry = new Zend_Form_Element('home_country');
        $homeCountry->setRequired(false);
        $this->addElement($homeCountry);
        
        $homeState = new Zend_Form_Element('home_state');
        $homeState->setRequired(false);
        $this->addElement($homeState);
        
        $homeCity = new Zend_Form_Element('home_city');
        $homeCity->setRequired(false);
        $this->addElement($homeCity);
        
        $homeStreet = new Zend_Form_Element('home_street');
        $homeStreet->setRequired(false);
        $this->addElement($homeStreet);
        
        $homeZipCode = new Zend_Form_Element('home_zip_code');
        $homeZipCode->setRequired(false);
        $this->addElement($homeZipCode);
        
        $marital = new Zend_Form_Element('marital');
        $marital->setRequired(false);
        $this->addElement($marital);
        
        $military = new Zend_Form_Element('military');
        $military->setRequired(false);
        $this->addElement($military);
        
        $militaryLoc = new Zend_Form_Element('military_loc');
        $militaryLoc->setRequired(false);
        $this->addElement($militaryLoc);
        
        $nationality = new Zend_Form_Element('nationality');
        $nationality->setRequired(false);
        $this->addElement($nationality);
        
        $religion = new Zend_Form_Element('religion');
        $religion->setRequired(false);
        $this->addElement($religion);
        
        $about = new Zend_Form_Element('about');
        $about->setRequired(false);
        $this->addElement($about);
    }
    
    public function isValid($data)
    {
        return parent::isValid($data);
    }
    
    public function getErrors($name = null, $suppressArrayNotation = false)
    {
        $errors     = parent::getErrors($name, $suppressArrayNotation);
        $messages   = $this->getMessages();
        
        $result = array();
        foreach ($errors as $field => $_errors) {
            if (!empty($_errors)) {
                $result[$field] = array(
                    'type'      => $_errors[0],
                    'message'   => $messages[$field][$_errors[0]],
                    'field'     => $field
                );
            }
        }
        
        return $result;
    }
}