<?php

class Users_Validate_ValidPassword extends Zend_Validate_Abstract
{
    const INVALID = 'invalid';
    
    protected $_messageTemplates = array(
        self::INVALID => "Password is invalid",
    );
    
    protected $_pass;
    
    public function __construct($pass = null)
    {
        $this->_pass = $pass;
    }
    
    public function isValid($value)
    {
        $acl = Zend_Registry::getInstance()->get('acl');
        $isAllowed = $acl->isAccessible('system', '*');
        
        if (!$isAllowed) {
            if ($value != $this->_pass) {
                $this->_error(self::INVALID);
                return false;
            }
        }
        
        return true;
    }
}
