<?php

class Users_Service_User extends Tea_Service_Abstract
{
    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Users_Model_DbTable_Users();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, Users_Model_User &$user = null)
    {
        if ($ret = $this->staticGet($id)) {
            if ($user instanceof Users_Model_User) {
                $user = $ret;
            }

            return $ret;
        }

        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$user instanceof Users_Model_User) {
            $user = new Users_Model_User();
        }
        $user->fill($row);
        $user->setNew(false);

        $this->staticSet($id, $user);
        return $user;
    }

    public function getByPKs($ids)
    {
        if (empty($ids)) {
            return array();
        }

        $rows = $this->_table->fetchAll(
            $this->_table->select()->where('id IN (?)', $ids)
        );

        $result = array();
        foreach ($rows as $row) {
            $user = new Users_Model_User();
            $user->fill($row);
            $user->setNew(false);

            $result[] = $user;
        }

        return $result;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;

        $cSelect->from(array('u' => 'users'), array('COUNT(*) AS count'));

        $select->from(array('u' => 'users'))
            ->joinLeft(
                array('p' => 'user_regions'),
                'u.profile_id = p.id',
                array(
                    'p.id'              => 'id',
                    'p.first_name_e'    => 'first_name_e',
                    'p.middle_name_e'   => 'middle_name_e',
                    'p.last_name_e'     => 'last_name_e',
                    'p.father_name'     => 'father_name',
                    'p.birth_loc'       => 'birth_loc',
                    'p.identity_no'     => 'identity_no',
                    'p.identity_date'   => 'identity_date',
                    'p.identity_loc'    => 'identity_loc',
                    'p.cell'            => 'cell',
                    'p.cell2'           => 'cell2',
                    'p.phone'           => 'phone',
                    'p.country'         => 'country',
                    'p.state'           => 'state',
                    'p.city'            => 'city',
                    'p.street'          => 'street',
                    'p.zip_code'        => 'zip_code',
                    'p.home_phone'      => 'home_phone',
                    'p.home_country'    => 'home_country',
                    'p.home_state'      => 'home_state',
                    'p.home_city'       => 'home_city',
                    'p.home_street'     => 'home_street',
                    'p.home_zip_code'   => 'home_zip_code',
                    'p.marital'         => 'marital',
                    'p.military'        => 'military',
                    'p.military_loc'    => 'military_loc',
                    'p.nationality'     => 'nationality',
                    'p.religion'        => 'religion',
                    'p.about'           => 'about',
                    'p.created_by_id'   => 'created_by_id',
                    'p.creation_date'   => 'creation_date',
                    'p.updated_by_id'   => 'updated_by_id',
                    'p.update_date'     => 'update_date'
                )
            );

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                if (Users_Model_User::hasProperty($key)) {
                    $key = "u.$key";

                } elseif (Users_Model_Region::hasProperty($key)) {
                    $key = "p.$key";

                } else {
                    $cSelect->joinLeft(array('s' => 'user_stats'), "u.id = s.user_id AND s.key = '$key'", array());
                    $select->joinLeft(array('s' => 'user_stats'), "u.id = s.user_id AND s.key = '$key'");
                    $key = "s.value";
                }

                $select->order($key . ' ' . strtoupper($ord));
            }
        }

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                switch ($key) {
                case 'name':
                    $select->where("CONCAT(u.first_name, u.last_name) LIKE '%$value%'");
                    $cSelect->where("CONCAT(u.first_name, u.last_name) LIKE '%$value%'");
                    break;

                case 'age':
                    if (is_array($value)) {
                        if (isset($value['from'])) {
                            $toDate = Tea_Model_Entity::convertAgeToDate($value['from']);
                            $select->where("birth_date <= '$toDate'");
                            $cSelect->where("birth_date <= '$toDate'");
                        }
                        if (isset($value['to'])) {
                            $fromDate = Tea_Model_Entity::convertAgeToDate($value['to']);
                            $select->where("birth_date >= '$fromDate'");
                            $cSelect->where("birth_date >= '$fromDate'");
                        }
                    }
                    break;

                default:
                    if (Users_Model_User::hasProperty($key)) {
                        $key = "u.$key";
                    } else if (Users_Model_Profile::hasProperty($key)) {
                        $key = "p.$key";
                    }

                    $select->where("{$key} = ?", $value);
                    $cSelect->where("{$key} = ?", $value);
                }
            }
        }

        $rows  = $this->_table->getAdapter()->fetchRow($cSelect);
        $count = (int)$rows['count'];

        $select->limit($limit, $start);

        $users     = array();
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {
            $userParams     = array();
            $profileParams  = array();

            foreach ($row as $col => $value) {
                $colName  = explode('.', $col);
                $prefix   = '';
                $property = '';

                if (count($colName) > 1) {
                    $prefix   = $colName[0];
                    $property = $colName[1];
                } elseif (count($colName) > 0) {
                    $prefix   = '';
                    $property = $colName[0];
                }

                switch ($prefix) {
                case 'p':
                    $profileParams[$property] = $value;
                    break;

                default:
                    $userParams[$property] = $value;
                }
            }

            $user = new Users_Model_User();
            $user->fill($userParams);
            $user->setNew(false);

            $profile = new Users_Model_Profile();
            $profile->fill($profileParams);
            $profile->setNew(false);

            $user->setProfile($profile);

            $users[] = $user;
        }

        return $users;
    }

    public function getByEmail($email)
    {
        $select = $this->_table->select();
        $select->where("email = ?", $email);
        
        $row = $this->_table->getAdapter()->fetchRow($select);
        if ($row) {
            $user = new Users_Model_User();
            $user->fill($row);
            $user->setNew(false);
            return $user;
        }
        return;
    }

    public function getUserByOauthId($provider, $id)
    {
        $networkService = Users_Service_Network::getInstance();

        $filter = array(
            'oauth_id' => $id,
            'provider' => $provider
        );
        $rows = $networkService->getList($filter, array(), 0, $count, 1);

        if (count($rows) == 0) {
            return null;
        } else {
            $rows = $this->_table->find($rows[0]->getUserId());
            if (count($rows) == 0) {
                return null;
            }
            $row = $rows->current()->toArray();
            $user = new Users_Model_User();

            $user->fill($row);
            $user->setNew(false);
            return $user;
        }
    }

    public function save(Users_Model_User $user)
    {
        if ($user->isNew()) {
            $data = $user->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                $this->getByPK($pk, $user);

                $this->staticSet($id, $user);

//                Tea_Hook_Registry::dispatchEvent('join_user', $user);
                return $user;
            }
        } else {
            $id = $user->getId();
            $this->staticDel($id);
            $data = $user->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            
            $this->getByPK($id, $user);
            return $user;
        }

        return false;
    }

    public function saveUserAndProfile(Users_Model_User $user, Users_Model_Profile $profile)
    {
        $profileService = Users_Service_Profile::getInstance();
        $authService    = Users_Service_Auth::getInstance();

        $isNew = $user->isNew();

        $this->_table->getAdapter()->beginTransaction();
        try {
            $this->save($user);
            $profileService->save($profile);

            if ($user->getProfileId() == null) {
                $user->setProfileId($profile->getId());
                $user->setNew(false);
                $user = $this->save($user);
            }

            $this->_table->getAdapter()->commit();
        } catch (Exception $e) {
            $this->_table->getAdapter()->rollBack();
            return $e->getMessage();
        }

        if (!$isNew) {
            $authService->saveCurrentUser($user, false);
            Tea_Hook_Registry::dispatchEvent('edit_user', $user);
        }

        return true;
    }

    public function remove(Users_Model_User $user)
    {
        $this->staticDel($user->getId());

        $followService      = Users_Service_Follow::getInstance();
        $networkService     = Users_Service_Network::getInstance();
        $prefService        = Users_Service_Pref::getInstance();
        $profileService     = Users_Service_Profile::getInstance();
        $userRoleService    = Users_Service_UserRole::getInstance();

        $this->_table->getAdapter()->beginTransaction();
        try {
            //------- delete related tables entries ---------
            $followService->removeByUser($user);
            $networkService->removeByUser($user);
            $prefService->removeByUser($user);
            $profileService->removeByUser($user);
            $userRoleService->removeByUser($user);
            //-----------------------------------------------

            $where = $this->_table->getAdapter()->quoteInto('id = ?', $user->getId());
            $this->_table->delete($where);

            $this->_table->getAdapter()->commit();
        } catch (Exception $e) {
            $this->_table->getAdapter()->rollBack();
            return $e->getMessage();
        }
    }

    public function removeAll()
    {
        $followService      = Users_Service_Follow::getInstance();
        $networkService     = Users_Service_Network::getInstance();
        $prefService        = Users_Service_Pref::getInstance();
        $profileService     = Users_Service_Profile::getInstance();
        $userRoleService    = Users_Service_UserRole::getInstance();

        $this->_table->getAdapter()->beginTransaction();
        try {
            //------- delete related tables entries ---------
            $followService->removeAll();
            $networkService->removeAll();
            $prefService->removeAll();
            $profileService->removeAll();
            $userRoleService->removeAll();
            //-----------------------------------------------

            $this->_table->delete('');

            $this->_table->getAdapter()->commit();
        } catch (Exception $e) {
            $this->_table->getAdapter()->rollBack();
            return $e->getMessage();
        }
    }

    public function getFollowing($user, $sort, $start, &$count, $limit=10)
    {
        $ids = Feeds_Service_Follow::getInstance()
            ->getFollowingIds($user->getId(), 'user', $sort, $start, $count, $limit);
        return $this->getByPKs($ids);
    }
    
    public function getDetails(Users_Model_User $user, $currentUser=null)
    {
        $result = $this->_getDetails($user);
        if (!$result) {
            return false;
        }
        
        $filter = array();
        if ($currentUser instanceof Users_Model_User) {
            $filter['or'] = array(
                'status'    => Sites_Model_Site::STATUS_PUBLISHED,
                'owner_id'  => $currentUser->getId()
            );
        } else {
            $filter['status'] = Sites_Model_Site::STATUS_PUBLISHED;
        }

        $sort = array(
            'follow.role'          => 'desc',
            'follow.creation_date' => 'desc'
        );

        $userSites = Sites_Service_Follow::getInstance()
            ->getUserSites($user, $filter, $sort, 0, $count, 5, null);

        $sites = array();
        foreach ($userSites as $userSite) {
            $_site = $userSite->toArray();
            unset($_site['style']);
            $sites[] = $_site;
        }

        $result['sites'] = $sites;

        if ($currentUser instanceof Users_Model_User) {
            $result = array_merge($result, $this->getUserRelatedInfo($user->getId(), $currentUser->getId()));
        }
        
        return $result;
    }

    private function _getDetails(Users_Model_User $user)
    {
        $result                        = $user->toArray(false, null, array('password'));

        $photo = $user->getPhoto();
        if ($photo instanceof Resources_Model_Resource) {
            $meta = $photo->getMetaData();
            $result['photo'] = array(
                'width'  => $meta['width'],
                'height' => $meta['height'],
                'link'   => $photo->getUrl()
            );
        }

        $profile = $user->getProfile();
        if ($profile instanceof Users_Model_Profile) {
            $_profile = $profile->toArray();
            $result = array_merge($result, $_profile);
            $result['id'] = $user->getId();
        }

        /*
         * Retrieve stats
         */
        $stats = Statistics_Service_ObjectStat::getInstance()
                ->getValues('user', $user->getId());
        $result = array_merge($result, $stats);

        return $result;
    }

    public function getUserRelatedInfo($userId, $currentUserId)
    {
        $result = array();
        $result['is_followed'] = Feeds_Service_Follow::getInstance()
                ->hasFollow('user', $userId, $currentUserId);
        
        return $result;
    }

}
