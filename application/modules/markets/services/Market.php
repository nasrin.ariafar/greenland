<?php

class Markets_Service_Market extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Markets_Model_DbTable_Markets();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, Markets_Model_Market $market = null)
    {

        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$market instanceof Markets_Model_Market) {
            $market = new Markets_Model_Market();
        }
        $market->fill($row);
        $market->setNew(false);
        return $market;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;

        $select->from(array('u' => 'markets'), array('*'));
        $cSelect->from(array('u' => 'markets'), array('COUNT(*) AS count'));

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $key = "u.$key";
                $select->order($key . ' ' . strtoupper($ord));
            }
        }

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                switch ($key) {
                    case 'updateDate':
                        $select->where("u.updateDate > '$value'");
                        $cSelect->where("u.updateDate > '$value'");
                        break;
                    default:
                        $key = "u.$key";
                        $select->where("{$key} = ?", $value);
                        $cSelect->where("{$key} = ?", $value);
                }
            }
        }

        $rows = $this->_table->getAdapter()->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $markets = array();
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {
            $item = new Markets_Model_Market();
            $item->fill($row);
            $markets[] = $item;
        }

        return $markets;
    }

    public function save(Markets_Model_Market $market)
    {
        if ($market->isNew()) {
            $data = $market->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($pk, $market);
            }
        } else {
            $id = $market->getId();
            $data = $market->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            return $this->getByPK($id, $market);
        }

        return false;
    }

    public function remove(Markets_Model_Market $market)
    {
        $where = $this->_table->getAdapter()->quoteInto('id = ?', $market->getId());
        $this->_table->delete($where);
    }

    public function removeByConditions($conditions = array())
    {
        $where = '';
        foreach ($conditions as $property => $value) {
            if (strlen($where) > 0) {
                $where .= ' AND ';
            }
            $where .= $this->_table->getAdapter()->quoteInto($property . ' = ?', $value);
        }

        $this->_table->delete($where);
    }

    public function removeByQuery($property, $query)
    {
        $this->_table->delete("$property IN ($query)");
    }

    public function removeAll()
    {
        $this->_table->delete('');
    }

}
